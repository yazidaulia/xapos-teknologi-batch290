package com.xapos.batch290.controller;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.xapos.batch290.model.Category;
import com.xapos.batch290.model.Variant;
import com.xapos.batch290.repository.CategoryRepository;
import com.xapos.batch290.repository.VariantRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiVariantController {
	@Autowired
	public VariantRepository variantRepository;
	
	@Autowired
	public CategoryRepository categoryRepository;
	
	@GetMapping("variant")
	public ResponseEntity<List<Variant>> getAllVariant(){
		try {
			List<Variant> variant = this.variantRepository.findByIsActive(true);
			return new ResponseEntity<>(variant, HttpStatus.OK);
		} catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("variant/{id}")
	public ResponseEntity<List<Category>> getCategoryById(@PathVariable("id") Long id) {
		try {
			Optional<Variant> variant = this.variantRepository.findById(id);
			if(variant.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(variant, HttpStatus.OK);
				return rest;
			}else {
				return ResponseEntity.notFound().build();
			}
		} catch(Exception e) {
			return new ResponseEntity<List<Category>>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("add/variant")
	public ResponseEntity<Object> saveVariant(@RequestBody Variant variant){
		variant.setCreatedBy("user1");
		variant.setCreatedDate(Date.from(Instant.now()));
		Variant variantData = this.variantRepository.save(variant);
		
		if(variantData.equals(variant)) {
			return new ResponseEntity<Object>("Save Data Successfully", HttpStatus.OK);
		}else {
			return new ResponseEntity<Object>("Failed Save Data", HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping("edit/variant/{id}")
	public ResponseEntity<Object> editCategory(@PathVariable("id") Long id,
			@RequestBody Variant variant){
		Optional<Variant> variantData = this.variantRepository.findById(id);
		if(variantData.isPresent()) {
			variant.setId(id);
			variant.setModifyBy("user1");
			variant.setModifyDate(Date.from(Instant.now()));
			variant.setCreatedBy(variantData.get().getCreatedBy());
			variant.setCreatedDate(variantData.get().getCreatedDate());
			this.variantRepository.save(variant);
			return new ResponseEntity<Object>("Updated Successfully", HttpStatus.OK);
		}else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@GetMapping("/variantbycategory/{id}")
	public ResponseEntity<List<Variant>> getAllVariantByCategory(@PathVariable("id") Long id) {
		try {
			List<Variant> variant = this.variantRepository.findBycategoryId(id);
			return new ResponseEntity<>(variant, HttpStatus.OK);
		} catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PutMapping("delete/variant/{id}")
	public ResponseEntity<Object> deleteVariant(@PathVariable("id") Long id){
		Optional<Variant> variantData = this.variantRepository.findById(id);
		
		if(variantData.isPresent()) {
			Variant variant = new Variant();
			variant.setId(id);
			variant.setIsActive(false);
			variant.setModifyBy("user1");
			variant.setModifyDate(Date.from(Instant.now()));
			variant.setCreatedBy(variantData.get().getCreatedBy());
			variant.setCreatedDate(variantData.get().getCreatedDate());
			variant.setVariantCode(variantData.get().getVariantCode());
			variant.setVariantName(variantData.get().getVariantName());
			this.variantRepository.save(variant);
			return new ResponseEntity<Object>("Deleted Successfully", HttpStatus.OK);
		}else {
			return ResponseEntity.notFound().build();
		}
	}
}
