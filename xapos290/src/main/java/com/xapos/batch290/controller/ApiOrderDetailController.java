package com.xapos.batch290.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xapos.batch290.model.OrderDetail;
import com.xapos.batch290.repository.OrderDetailRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiOrderDetailController {

	@Autowired
	public OrderDetailRepository orderDetailRepository;
	
	@PostMapping("orderdetail")
	public ResponseEntity<Object> saveOrderDetail(@RequestBody OrderDetail orderDetail){
		OrderDetail orderDetailData = this.orderDetailRepository.save(orderDetail);
		
		if(orderDetailData.equals(orderDetail)) {
			return new ResponseEntity<Object>("Save Item Success", HttpStatus.OK);
		}else {
			return new ResponseEntity<Object>("Failed Save Data", HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("orderdetailbyorderheader/{headerId}")
	public ResponseEntity<List<OrderDetail>> getAllOrderDetailByIdOrderHeader(
			@PathVariable("headerId") Long headerId){
		try {
			List<OrderDetail> orderDetails = this.orderDetailRepository.findByHeaderId(headerId);
			return new ResponseEntity<List<OrderDetail>>(orderDetails, HttpStatus.OK);
		} catch(Exception e) {
			return new ResponseEntity<List<OrderDetail>>(HttpStatus.NO_CONTENT);
		}
	}
}
