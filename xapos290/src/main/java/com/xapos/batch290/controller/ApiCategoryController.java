package com.xapos.batch290.controller;

import java.awt.print.Pageable;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xapos.batch290.model.Category;
import com.xapos.batch290.repository.CategoryRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiCategoryController {
	
	@Autowired
	public CategoryRepository categoryRepository;
	
	@GetMapping("category")
	public ResponseEntity<List<Category>> getAllCategory(){
		try {
			List<Category> category = this.categoryRepository.findByIsActive(true);
			return new ResponseEntity<>(category, HttpStatus.OK);
		} catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("category/{id}")
	public ResponseEntity<List<Category>> getCategoryById(@PathVariable("id") Long id) {
		try {
			Optional<Category> category = this.categoryRepository.findById(id);
			if(category.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(category, HttpStatus.OK);
				return rest;
			}else {
				return ResponseEntity.notFound().build();
			}
		} catch(Exception e) {
			return new ResponseEntity<List<Category>>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("add/category")
	public ResponseEntity<Object> saveCategory(@RequestBody Category category){
		category.setCreatedBy("user1");
		category.setCreatedDate(Date.from(Instant.now()));
		Category categoryData = this.categoryRepository.save(category);
		
		if(categoryData.equals(category)) {
			return new ResponseEntity<Object>("Save Data Successfully", HttpStatus.OK);
		}else {
			return new ResponseEntity<Object>("Save Failed", HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping("edit/category/{id}")
	public ResponseEntity<Object> editCategory(@PathVariable("id") Long id, 
			@RequestBody Category category){
		//Untuk isian data sementara, digunakan untuk menyalin isi data sebelumnya
		Optional<Category> categoryData = this.categoryRepository.findById(id);
		if(categoryData.isPresent()) {
			category.setId(id);
			category.setModifyBy("user1");
			category.setModifyDate(Date.from(Instant.now()));
			category.setCreatedBy(categoryData.get().getCreatedBy());
			category.setCreatedDate(categoryData.get().getCreatedDate());
			this.categoryRepository.save(category);
			return new ResponseEntity<Object>("Updated Successfully", HttpStatus.OK);
		}else {
			//menampilkan lembar HTTP Status not Found
			return ResponseEntity.notFound().build();
		}
	}
	
	@PutMapping("delete/category/{id}")
	public ResponseEntity<Object> deleteCategory(@PathVariable("id") Long id){
		Optional<Category> categoryData = this.categoryRepository.findById(id);
		
		if(categoryData.isPresent()) {
			Category category = new Category();
			category.setId(id);
			category.setIsActive(false);
			category.setModifyBy("user1");
			category.setModifyDate(Date.from(Instant.now()));
			category.setCreatedBy(categoryData.get().getCreatedBy());
			category.setCreatedDate(categoryData.get().getCreatedDate());
			category.setCategoryCode(categoryData.get().getCategoryCode());
			category.setCategoryName(categoryData.get().getCategoryName());
			this.categoryRepository.save(category);
			return new ResponseEntity<Object>("Deleted Successfully", HttpStatus.OK);
		}else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@PostMapping("category/search/")
	public ResponseEntity<List<Category>> getCategoryByName(@RequestParam("keyword") String keyword){
		if(keyword.equals("")){
			List<Category> category = this.categoryRepository.findByIsActive(true);
			return new ResponseEntity<List<Category>>(category, HttpStatus.OK);
		} else {
			List<Category> category = this.categoryRepository.searchByKeyword(keyword);
			return new ResponseEntity<List<Category>>(category, HttpStatus.OK);
		}
	}
	
	@GetMapping("category/paging")
	public ResponseEntity<Map<String, Object>> getAllCategories(@RequestParam(defaultValue = "0") int page,
			@RequestParam(defaultValue = "5") int size){
		try {
			List<Category> category = new ArrayList<>();
			PageRequest pagingSort = PageRequest.of(page, size);
			
			Page<Category> pageTuts;
			pageTuts = this.categoryRepository.findAll(pagingSort);
			
			category = pageTuts.getContent();
			
			Map<String, Object> response = new HashMap<>();
			response.put("category", category);
			response.put("currentPage", pageTuts.getNumber());
			response.put("totalItems", pageTuts.getTotalElements());
			response.put("totalPage", pageTuts.getTotalPages());
			
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}