package com.xapos.batch290.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xapos.batch290.model.OrderHeader;
import com.xapos.batch290.repository.OrderHeaderRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiOrderHeaderController {

	@Autowired
	public OrderHeaderRepository orderHeaderRepository;
	
	@GetMapping("showorderheader")
	public ResponseEntity<List<OrderHeader>> getAllOrderHeader(){
		try {
			List<OrderHeader> orderHeader = this.orderHeaderRepository.findByAmount();
			return new ResponseEntity<>(orderHeader, HttpStatus.OK);
		} catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("orderheader")
	public ResponseEntity<Object> createdReference(@RequestBody OrderHeader orderHeader){
		String timeDec = System.currentTimeMillis() + "";
		orderHeader.setReference(timeDec);
		
		OrderHeader orderHeaderData = this.orderHeaderRepository.save(orderHeader);
		
		if(orderHeaderData.equals(orderHeader)) {
			return new ResponseEntity<Object>("Save Data Successfully", HttpStatus.OK);
		}else {
			return new ResponseEntity<Object>("Failed Save Data", HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("orderheadermaxid") 
	public ResponseEntity<Long> getMaxOrderHeader(){
		try {
			Long orderHeader = this.orderHeaderRepository.findByMaxId();
			return new ResponseEntity<Long>(orderHeader, HttpStatus.OK);
		} catch(Exception e) {
			return new ResponseEntity<Long>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PutMapping("done/orderheader")
	public ResponseEntity<Object> doneProcess(@RequestBody OrderHeader orderHeader) {
		Long id = orderHeader.getId();
		
		Optional<OrderHeader> orderHeaderData = this.orderHeaderRepository.findById(id);
		if(orderHeaderData.isPresent()) {
			orderHeader.setId(id);
			this.orderHeaderRepository.save(orderHeader);
			return new ResponseEntity<Object>("Order Success", HttpStatus.OK);
		}else {
			return ResponseEntity.notFound().build();
		}
		
	}
}