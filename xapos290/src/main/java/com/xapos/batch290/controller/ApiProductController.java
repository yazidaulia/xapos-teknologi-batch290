package com.xapos.batch290.controller;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xapos.batch290.model.Variant;
import com.xapos.batch290.model.Product;
import com.xapos.batch290.repository.CategoryRepository;
import com.xapos.batch290.repository.ProductRepository;
import com.xapos.batch290.repository.VariantRepository;

@RestController
@CrossOrigin("*") 
@RequestMapping("/api")
public class ApiProductController {
	@Autowired
	public ProductRepository productRepository;
	
	@Autowired
	public VariantRepository variantRepository;
	
	@GetMapping("product")
	public ResponseEntity<List<Product>> getAllProduct(){
		try {
			List<Product> product = this.productRepository.findByIsActive(true);
			return new ResponseEntity<>(product, HttpStatus.OK);
		} catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("product/{id}")
	public ResponseEntity<List<Variant>> getVariantById(@PathVariable("id") Long id){
		try {
			Optional<Product> product = this.productRepository.findById(id);
			if(product.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(product, HttpStatus.OK);
				return rest;
			}else {
				return ResponseEntity.notFound().build();
			}
		} catch(Exception e) {
			return new ResponseEntity<List<Variant>>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("add/product")
	public ResponseEntity<Object> saveProduct(@RequestBody Product product){
		product.setCreatedBy("user1");
		product.setCreatedDate(Date.from(Instant.now()));
		Product productData = this.productRepository.save(product);
		
		if(productData.equals(product)) {
			return new ResponseEntity<Object>("Save Data Successfully", HttpStatus.OK);
		}else {
			return new ResponseEntity<Object>("Failed Save Data", HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping("edit/product/{id}")
	public ResponseEntity<Object> editProduct(@PathVariable("id") Long id,
			@RequestBody Product product){
		Optional<Product> productData = this.productRepository.findById(id);
		if(productData.isPresent()) {
			product.setId(id);
			product.setModifyBy("user1");
			product.setModifyDate(Date.from(Instant.now()));
			product.setCreatedBy(productData.get().getCreatedBy());
			product.setCreatedDate(productData.get().getCreatedDate());
			this.productRepository.save(product);
			return new ResponseEntity<Object>("Updated Successfully", HttpStatus.OK);
		}else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@PutMapping("delete/product/{id}")
	public ResponseEntity<Object> deleteProduct(@PathVariable("id") Long id){
		Optional<Product> productData = this.productRepository.findById(id);
		
		if(productData.isPresent()) {
			Product product = new Product();
			product.setId(id);
			product.setIsActive(false);
			product.setModifyBy("user1");
			product.setModifyDate(Date.from(Instant.now()));
			product.setCreatedBy(productData.get().getCreatedBy());
			product.setCreatedDate(productData.get().getCreatedDate());
			product.setProductCode(productData.get().getProductCode());
			product.setProductName(productData.get().getProductName());
			this.productRepository.save(product);
			return new ResponseEntity<Object>("Deleted Successfully", HttpStatus.OK);
		}else {
			return ResponseEntity.notFound().build();
		}
	}
	
}
