package com.xapos.batch290.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xapos.batch290.model.Category;
import com.xapos.batch290.model.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {
	List<Product> findByIsActive(Boolean isActive);
}
