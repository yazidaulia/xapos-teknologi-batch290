package com.xapos.batch290.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.xapos.batch290.model.Variant;

public interface VariantRepository extends JpaRepository<Variant, Long> {
	List<Variant> findBycategoryId(Long categoryId);
	
	List<Variant> findByIsActive(Boolean isActive);
}
