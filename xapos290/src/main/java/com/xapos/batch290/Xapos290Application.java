package com.xapos.batch290;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Xapos290Application {

	public static void main(String[] args) {
		SpringApplication.run(Xapos290Application.class, args);
	}

}
